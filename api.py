from config import mysql, app

# "request" enable us to access the json object
from flask import request

# # # REQUEST EXAMPLE
# # {
# #   "node_id": 5,
# #   "language": "english",
# #   "search_keyword": "",
# #   "page_num": 1,
# #   "page_size": 3
# # }

@app.route('/')
def index():
    return {'get node_tree sample': '/node_tree',
            'get node_tree_names sample': '/node_tree_names',
            'get descendents through json object': '/descendents'
            }

@app.route('/node_tree')
def node_tree():
    cur = mysql.connection.cursor(dictionary=True)
    cur.execute("SELECT * FROM node_tree LIMIT 1000")
    output = cur.fetchall()
    # print(output)
    return {'node_tree':output}

@app.route('/node_tree_names')
def node_tree_names():
    cur = mysql.connection.cursor(dictionary=True)
    cur.execute("SELECT * FROM node_tree_names LIMIT 1000")
    output = cur.fetchall()
    return {'node_tree_names':output}


# get iLeft and iRight for the selected node
# INPUT: idNode
# OUTPUT: (iLeft, iRight) OR ([],[])
def get_LR(id):
    cur = mysql.connection.cursor(dictionary=True)
    cur.execute(f"SELECT iLeft, iRight FROM node_tree WHERE idNode = {id}")
    x = cur.fetchall()
    # if the node id is not found return 2 empty lists
    return (x[0]['iLeft'], x[0]['iRight']) if x else ([],[])

# count the children for each node
# INPUT: idNode
# OUTPUT: count
def children_count(id):
    cur = mysql.connection.cursor(dictionary=True)
    l,r = get_LR(id)
    if l and r:
        cur.execute(f'''SELECT COUNT(idNode) AS count FROM node_tree
                         WHERE iLeft BETWEEN {l} AND {r}
                         AND NOT idNode = {id}''')
        output = cur.fetchall()
        return output[0]['count']
    else:
        return None

# GET ALL THE DESCENDENTS FROM A NODE
# INPUT: a json through GET method with the following params:
#       node_id (integer, required): the unique ID of the selected node.
#       language (enum, required): language identifier. Possible values: "english", "italian".
#       search_keyword (string, optional): a search term used to filter results. If provided, restricts
#                       the results to "all children nodes under node_id whose nodeName in the given language
#                       contains search_keyword (case insensitive)".
#       page_num (integer, optional): the 0-based identifier of the page to retrieve. If not
#                 provided, defaults to “0”.
#       page_size (integer, optional): the size of the page to retrieve, ranging from 0 to 1000. If
#                  not provided, defaults to “100”.
# OUTPUT: a JSON with the following fields:
#       nodes (array, required): 0 or more nodes matching the given conditions.
#           Each node contains:
#               node_id (integer, required): the unique ID of the child node.
#               name (string, required): the node name translated in the requested language.
#               children_count (integer, required): the number of child nodes of this node.
#       error (string, optional): If there was an error, return the generated message.
@app.route('/descendents')
def get_descendents():
    # get the json from the GET request
    try:
        id = int(request.json['node_id'])               #int, required
    except KeyError:
        return {"error":"Missing mandatory params"}

    try:
        lang = str(request.json['language'])            #enum, required
    except KeyError:
        return {"error":"Missing mandatory params"}
    else:
        if lang.lower() != "english" and lang.lower() != "italian":
            return {"error":"Language possible values: 'english', 'italian'"}

    try:
        search = str(request.json['search_keyword'])     #str, optional
    except KeyError:
        # set default value if 'search_keyword' is not provided in the json
        search = ''

    try:
        offset = int(request.json['page_num'])          #int, optional
    except KeyError:
        # set default value if 'page_num' is not provided in the json
        offset = 0
    else:
        if offset<0:
            return {"error":"Invalid page number requested"}

    try:
        size = int(request.json['page_size'])           #int, optional
    except KeyError:
        # set default value if 'page_size' is not provided in the json
        size = 100
    else:
        if size<0 or size>1000:
            return {"error":"Invalid page size requested"}


    ## get iLeft and iRight for the selected node
    l,r = get_LR(id)
    if l and r:                         #check if the id is valid
        cur = mysql.connection.cursor(dictionary=True)
        # main query
        try:
            cur.execute(f'''SELECT idNode, nodeName FROM node_tree_names
                            WHERE
                            language = "{lang}" AND
                            nodeName LIKE "%{search}%" AND
                            idNode IN (SELECT idNode FROM node_tree
                                       WHERE iLeft > {l} AND iRight < {r})
                            LIMIT {offset*size},{size}''')
            raw_output = cur.fetchall()
            output =[]
            for y in raw_output:
                c_count = children_count(y['idNode'])
                output.append({"node_id": y['idNode'], "name": y['nodeName'],
                               "children_count":c_count})
            return {'nodes':output}
        except BaseException as e:
            return {'error': str(e)}
    else:
        return {"error":"Invalid node id"}
