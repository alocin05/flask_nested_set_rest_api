from flask import Flask

#it provides a connection to the MySQL database ensuring plain SQL readability 
from flask_mysql_connector import MySQL

app = Flask(__name__)
app.config['MYSQL_USER']='user1'
app.config['MYSQL_PASSWORD']='password'
app.config['MYSQL_DATABASE']='REST_ex'
app.config['MYSQL_HOST'] ='localhost'
mysql = MySQL(app)
