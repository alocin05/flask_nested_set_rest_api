CREATE TABLE node_tree(
  idNode TINYINT UNSIGNED,
  level TINYINT UNSIGNED NOT NULL,
  iLeft TINYINT UNSIGNED NOT NULL,
  iRight TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (idNode)
);


CREATE TABLE node_tree_names(
  idNode TINYINT UNSIGNED NOT NULL,
  language ENUM('english','italian') NOT NULL,
  nodeName VARCHAR(25) NOT NULL,
  FOREIGN KEY (idNode) REFERENCES node_tree(idNode)
);
